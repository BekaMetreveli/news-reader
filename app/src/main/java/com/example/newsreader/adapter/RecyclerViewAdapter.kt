package com.example.newsreader.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsreader.R
import com.example.newsreader.viewmodel.NewsViewModel
import kotlinx.android.synthetic.main.adapter_item.view.*

class RecyclerViewAdapter(private var adapterItems: List<NewsViewModel.NewsViewData>?) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount(): Int {
        return adapterItems?.size ?: 0
    }

    fun setAdapterData(adapterItems: List<NewsViewModel.NewsViewData>) {
        this.adapterItems = adapterItems
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var item: NewsViewModel.NewsViewData
        fun onBind() {
            item = adapterItems?.get(adapterPosition) ?: NewsViewModel.NewsViewData()

            itemView.tv_title.text = item.title
            itemView.tv_description.text = item.description
            Glide.with(itemView.iv_image).load(item.image).into(itemView.iv_image)

            itemView.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(item.url)
                startActivity(itemView.context, intent, null)
            }
        }
    }
}