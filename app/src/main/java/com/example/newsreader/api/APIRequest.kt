package com.example.newsreader.api

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface APIRequest {
    @GET("/v1/latest-news?language=en&apiKey=KK-Ch0M_gl4ZJZm7uye9aMH8x8rZJ-jdQkN16vaSqicPBmRo")
    fun getNews(): Call<APIResponse>

    companion object {
        val instance: APIRequest by lazy {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.currentsapi.services")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            retrofit.create<APIRequest>(APIRequest::class.java)
        }
    }
}