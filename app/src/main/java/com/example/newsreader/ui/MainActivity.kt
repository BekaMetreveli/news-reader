package com.example.newsreader.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsreader.R
import com.example.newsreader.adapter.RecyclerViewAdapter
import com.example.newsreader.api.APIRequest
import com.example.newsreader.repo.NewsRepo
import com.example.newsreader.viewmodel.NewsViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var newsViewModel: NewsViewModel
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        newsViewModel = ViewModelProviders.of(this).get(NewsViewModel::class.java)
        newsViewModel.newsRepo = NewsRepo(APIRequest.instance)

        adapter = RecyclerViewAdapter(null)


        newsViewModel.getNews {
            adapter.setAdapterData(it.news)
        }

        rv_recyclerView.layoutManager = LinearLayoutManager(this)
        rv_recyclerView.adapter = adapter

        v_blackScreen.animate().apply {
            alpha(0f)
            duration = 3000
        }.start()

        progressBar.animate().apply {
            alpha(0f)
            duration = 3000
        }.start()
    }
}