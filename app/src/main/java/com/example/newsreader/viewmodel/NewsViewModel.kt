package com.example.newsreader.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.newsreader.api.APIResponse
import com.example.newsreader.repo.NewsRepo

class NewsViewModel(application: Application) : AndroidViewModel(application) {
    var newsRepo: NewsRepo? = null

    fun getNews(callback: (APIResponseViewData) -> Unit) {
        newsRepo?.getNews {
            val apiResponseViewData = responseToViewData(it)
            callback(apiResponseViewData)
        }
    }

    private fun responseToViewData(apiResponse: APIResponse): APIResponseViewData {
        return APIResponseViewData(
            newsListToNewsViewData(apiResponse.news),
            apiResponse.page,
            apiResponse.status
        )
    }

    private fun newsListToNewsViewData(newsList: List<APIResponse.New>): List<NewsViewData> {
        return newsList.map {
            NewsViewData(
                it.author,
                it.category,
                it.description,
                it.id,
                it.image,
                it.language,
                it.published,
                it.title,
                it.url
            )
        }
    }

    data class APIResponseViewData(
        val news: List<NewsViewData> = emptyList(),
        val page: Int = 0,
        val status: String = ""
    )

    data class NewsViewData(
        val author: String = "",
        val category: List<String> = emptyList(),
        val description: String = "",
        val id: String = "",
        val image: String = "",
        val language: String = "",
        val published: String = "",
        val title: String = "",
        val url: String = ""
    )
}