package com.example.newsreader.repo

import android.widget.Toast
import com.example.newsreader.ui.MainActivity
import com.example.newsreader.api.APIRequest
import com.example.newsreader.api.APIResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsRepo(private val apiRequest: APIRequest) {

    fun getNews(callBack: (APIResponse) -> Unit) {

        val newsCall = apiRequest.getNews()

        newsCall.enqueue(object : Callback<APIResponse> {
            override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {
                val body = response.body()
                if (body != null) {
                    callBack(body)
                }
            }

            override fun onFailure(call: Call<APIResponse>, t: Throwable) {
                Toast.makeText(MainActivity(), "${t.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }
}